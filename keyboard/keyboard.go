package keyboard

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	tgbotapi "github.com/Syfaro/telegram-bot-api"
)

const baseURL = "https://api.telegram.org/bot5907484224:AAGyf0jK4NbJwiQTQq7leBvOqChnLwV7R4Y/"

type sendMessageRequest struct {
	ChatID      int64                        `json:"chat_id"`
	Text        string                       `json:"text"`
	ReplyMarkup tgbotapi.ReplyKeyboardMarkup `json:"reply_markup"`
}

func Set(chatID int64, text string, buttons tgbotapi.ReplyKeyboardMarkup) error {
	// создаем объект запроса на отправку сообщения
	request, err := json.Marshal(sendMessageRequest{
		ChatID:      chatID,
		Text:        text,
		ReplyMarkup: buttons,
	})
	if err != nil {
		return fmt.Errorf("marshal err: %w", err)
	}

	// отправляем запрос на отправку сообщения
	response, err := http.Post(baseURL+"sendMessage", "application/json", bytes.NewBuffer(request))
	if err != nil {
		return fmt.Errorf("send err: %w", err)
	}
	defer response.Body.Close()

	// проверяем статус ответа
	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("request failed")
	}

	return nil
}

func Create(msg string) (tgbotapi.ReplyKeyboardMarkup, bool) {
	var buttons tgbotapi.ReplyKeyboardMarkup
	ok := true

	switch {
	case msg == "/start" || msg == "назад" || msg == "наверх":
		buttons = tgbotapi.NewReplyKeyboard(
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("button1"),
				tgbotapi.NewKeyboardButton("button2"),
			),
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("button3"),
				tgbotapi.NewKeyboardButton("button4"),
			),
		)
	case msg == "button1":
		buttons = tgbotapi.NewReplyKeyboard(
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("назад"),
				tgbotapi.NewKeyboardButton("наверх"),
			),
		)
	default:
		ok = false
	}

	return buttons, ok
}
