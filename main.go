package main

import (
	"log"

	tgbotapi "github.com/Syfaro/telegram-bot-api"
	"gitlab.com/test_progect/mini_telegram_bot/keyboard"
)

func main() {
	// подключаемся к боту с помощью токена
	bot, err := tgbotapi.NewBotAPI("5907484224:AAGyf0jK4NbJwiQTQq7leBvOqChnLwV7R4Y")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = false
	log.Printf("Authorized on account %s", bot.Self.UserName)

	// инициализируем канал, куда будут прилетать обновления от API
	var ucfg tgbotapi.UpdateConfig = tgbotapi.NewUpdate(0)
	ucfg.Timeout = 60
	upd, _ := bot.GetUpdatesChan(ucfg)
	// читаем обновления из канала
	for update := range upd {
		// Пользователь, который написал боту
		UserName := update.Message.From.UserName

		// ID чата/диалога.
		// Может быть идентификатором как чата с пользователем
		// (тогда он равен UserID) так и публичного чата/канала
		ChatID := update.Message.Chat.ID

		// Текст сообщения
		Text := update.Message.Text
		log.Printf("[%s] %d %s", UserName, ChatID, Text)

		// Ответим пользователю его же сообщением
		reply := Text
		// Созадаем сообщение
		msg := tgbotapi.NewMessage(ChatID, reply)
		// и отправляем его
		bot.Send(msg)

		buttons, ok := keyboard.Create(reply)
		if !ok {
			continue
		}

		if err := keyboard.Set(ChatID, "text", buttons); err != nil {
			log.Printf("set new keyboard err: %v", err)
		}
	}
}
