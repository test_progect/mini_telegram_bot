# mini_telegram_bot

минимальный телеграмм бот

Прежде чем начать работать с ботом, его необходимо создать. Для этого заходим в Telegram и находим там пользователя @BotFather и отсылаем ему команду /start, после чего он выдаст список доступных команд.
Нас интересует команда /newbot. Запускаем её и отвечаем на несколько вопросов.
Когда регистрация бота завершится успешно, вы увидите сообщение со ссылкой на вашего бота и токен для доступа к нему через API.